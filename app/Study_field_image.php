<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Study_field_image extends Model
{
    protected $fillable = [
        'name', 'study_field_id', 'icon_path',
    ];
}
