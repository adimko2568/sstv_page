<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Page_image extends Model
{
    protected $fillable = [
        'name', 'study_field_id',
    ];
}
