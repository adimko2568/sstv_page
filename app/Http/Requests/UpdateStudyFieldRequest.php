<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudyFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title.required' => 'Odbor musí obsahovať nadpis',
            'title.max' => 'Nadpis može byť dlhý maximálne 255 znakov',
            'content.required'  => 'Odbor musí mať obsah',
            'icon.image' => 'Inkonka musí byť obrázok',
            'images.*.image' => ' Všetky obrázky vkladané do galérie musia byť v správnom formáte'

        ];
    }
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => 'required',
            'icon' => 'image',
            'images.*' => 'image'
        ];
    }
}
