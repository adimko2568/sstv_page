<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLinkSectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Sekcia musí mať názov',
            'title.max' => 'Názov musí byť menší ako 255 znakov'
            
            
            

        ];
    }
    public function rules()
    {
        return [
            'title' => 'required|max:255'
        ];
    }
}
