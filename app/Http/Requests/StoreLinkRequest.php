<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Link musí mať názov',
            'name.max' => 'Názov musí byť menší ako 255 znakov',
            'linksection_id.required' => 'Súbor musí byť viazaný na sekciu',
            'file.required_without' => 'Vyberte súbor alebo zadajte link',
            'file.max' => 'Súbor môže byť maximálne 50 MB veľký',
            'url.required_without' => 'Zadajde link alebo vyberte súbor'
            
            

        ];
    }
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'url' => 'required_without:file',
            'linksection_id' => 'required',
            'file' => 'required_without:url|max:500000|file'
           
        ];
    }
}
