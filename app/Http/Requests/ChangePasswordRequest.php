<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'oldPassword.required' => 'Musíte vyplniť staré heslo',
            'newPassword.required' => 'Musíte vyplniť nové heslo',
            'newPassword2.required'  => 'Musíte zopakovať nové heslo',
            'newPassword2.same' => 'Nové heslá sa nezhodujú'
        ];
    }

    public function rules()
    {
        return [
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'newPassword2' => 'required|same:newPassword'
            
        ];
    }
}
