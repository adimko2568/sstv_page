<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileSectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'sectiontitle.required' => 'Sekcia musí mať svoj názov',
            'sectiontitle.max' => 'Sekcia môže byť dlhá maximálne 255 znakov',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [     
            'sectiontitle' => 'required|max:255',
        ];
    }
}
