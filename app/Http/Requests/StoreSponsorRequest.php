<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSponsorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sponsorUrl.required' => 'Sponzor musí mať url adresu',
            'img.image' => 'Obrázok nie je v správnom formáte',
            'img.required' => 'Sponzor musí mať logo'
            
            

        ];
    }
    public function rules()
    {
        return [
            'sponsorUrl' => 'required',
            'img' => 'image|required'
        ];
    }
}
