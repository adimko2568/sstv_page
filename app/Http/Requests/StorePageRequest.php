<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'pageTitle.required' => 'Stránka musí mať názov',
            'pageTitle.max' => 'Názov musí byť menší ako 255 znakov',
            'content.requied' => 'Stránka musí mať obsah',
            'static_menu_item.required' => 'Stránka musí byť uložená pod sekciuo',
            'images.*.image' => 'Obrázky nie sú v správnom formáte',
            'file.max' => 'Súbor môže byť maximálne 50 MB veľký',
            
            

        ];
    }
    public function rules()
    {
        return [
            'content' => 'required',
            'pageTitle' => 'required|max:255',
            'static_menu_item' => 'required',
            'images.*' => 'image',
            'files.*' => 'max:500000|file'
        ];
    }
}
