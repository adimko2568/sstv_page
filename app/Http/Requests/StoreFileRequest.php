<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Súbor musí mať názov',
            'name.max' => 'Názov musí byť menší ako 255 znakov',
            'section.required' => 'Súbor musí byť viazaný na sekciu',
            'file.required' => 'Vyberte prosím súbor',
            'file.max' => 'Súbor môže byť maximálne 50 MB veľký'
            
            

        ];
    }
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'section' => 'required',
            'file' => 'required|max:500000'
           
        ];
    }
}
