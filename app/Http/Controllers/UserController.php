<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ChangeNameRequest;
use Auth;
use Hash;
class UserController extends Controller
{
    public function showChangeName(){
        return view('admin.admin-changeName', ['user' => Auth::user()]);
    }

    public function showChangePassword(){
        return view('admin.admin-changePassword');
    }

    public function changeName(ChangeNameRequest $request){
        $user = User::findOrFail(Auth::user()->id);
        $user->name = $request->name;
        $user->save();
        return back()->withSuccess('Meno bolo úspešne zmenené');
    }

    public function showUsers(){
        return view('admin.admin-users', ['users' => User::where('role', '!=', 'admin')->get()]);
    }
    public function destroyUser(Request $request){
        $user = User::findOrFail($request->id);
        $user->delete();
        return back()->withSuccess('Účet bol úspešne vymazaný');
    }

    public function changePassword(ChangePasswordRequest $request){
        if(Hash::check($request->oldPassword, Auth::user()->password)){
            $user = User::findOrFail(Auth::user()->id);
            $user->password=Hash::make($request->newPassword);
            $user->save();
            return back()->withSuccess('Heslo bolo úspešne zmenené');
        }
        else return back()->withErrors('Staré heslo sa nezhoduje');
    }
}
