<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Http\Request;
use App\Post;
use App\Post_image;

class PostController extends Controller
{
    public function show_one($id){
        return view('post', ['post' => Post::findOrFail($id)]);
    }
    public function index()
    {
        $posts = Post::paginate(18);
        return view('admin.admin-posts', ['posts' => $posts]);
    }
    public function show_all()
    {
        $posts = Post::paginate(15);
        return view('prispevky', ['posts' => $posts]);
    }
    public function showAddPost()
    {
        return view('admin.add-post');
    }
    public function storePost(PostRequest $request){
        $destinationPath = 'public/images/postImages';
        $post = new Post;
        $image = new Post_image;
        if($request->hasFile('titleImage')) {
            $filename=time().$request->file('titleImage')->getClientOriginalName();
            $request->file('titleImage')->move($destinationPath, $filename);
            $post->title_image = $filename;
        }
        $post->title=$request->title;
        $post->content=$request->content;
        
        $post->save();
        if($request->hasFile('images')) {
            foreach($request->file('images') as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $size = $image->getSize();
                $ext = $image->extension();
                $image->move($destinationPath, $filename);
                $image = new Post_image;
                $image->name=$filename;
                $image->size=$size;
                $image->ext=$ext;
                $image->post_id=$post->id;
                $image->save();
            }
        }        
        
        return back()->withSuccess('Prispevok bol úspešne pridaný');
    }
    public function destroyImage(Request $request){
        
        $post_image = new Post_image;
        $post_image = Post_image::findOrFail($request->id);
                 
            if(is_file('public/images/postImages/'.$post_image->name)){
                unlink('public/images/postImages/'.$post_image->name);
                $post_image->delete();
        }
        return back()->withSuccess('Obrazok bol vymazaný');
    }
    public function destroyPost(Request $request){
        $post = new Post;
        $post = Post::findOrFail($request->id);
        

        foreach($post->images as $image){           
            $image = 'public/images/postImages/'.$image->name;
            if(is_file($image)){
                unlink($image);
        }
        
        }
        
        $mainImagePath = 'public/images/postImages/'.$post->title_image;
        if(is_file($mainImagePath)){
            unlink($mainImagePath);
        }
        $post->delete();

        return back()->withSuccess('Príspevok bol úspešne vymazaný');
    }
    public function showEditPost(Request $request){
        return view('admin.add-post', ['post' => Post::findOrFail($request->id)]);
    }

    public function editPost(UpdatePostRequest $request){
        $destinationPath = 'public/images/postImages';
        $post = Post::findOrFail($request->id);
        $post->title = $request->title;
        $post->content=$request->content;
        if($request->hasFile('images')) {
            foreach($request->file('images') as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $size = $image->getSize();
                $ext = $image->extension();
                $image->move($destinationPath, $filename);
                $image = new Post_image;
                $image->name=$filename;
                $image->size=$size;
                $image->ext=$ext;
                $image->post_id=$post->id;
                $image->save();
            }
        } 
        if($request->hasFile('titleImage')) {
            
            $mainImagePath = 'public/images/postImages/'.$post->title_image;
            if(is_file($mainImagePath)){
                unlink($mainImagePath);
            }
            $filename=time().$request->file('titleImage')->getClientOriginalName();
            $request->file('titleImage')->move($destinationPath, $filename);
            $post->title_image = $filename;
        }
        $post->save();
        return back()->withSuccess('Príspevok bol úspešne zmenený');
    }

}