<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Post;
use App\Post_image;
use App\Study_field;
use App\Sponsor;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('homePage', ['posts' => Post::orderBy('created_at', 'desc')->get(), 'study_fields' => Study_field::get(), 'sponsors' => Sponsor::get() ]);
    }
}
