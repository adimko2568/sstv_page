<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreStudyFieldRequest;
use App\Http\Requests\UpdateStudyFieldRequest;
use App\Study_field;
use App\Study_field_image;


class Study_fieldController extends Controller
{

    public function showField($slug){

        return view('field', ['field'=> Study_field::where('slug', $slug)->firstorfail()]);
    }
    public function showAll(){
        return view ('admin.admin-study_field', ['study_fields'=>Study_field::paginate(15)]);
    }

    public function showAddStudyField(){
        return view('admin.add-study_field');
    }

    public function showEditStudyField($id){
        return view('admin.add-study_field', ['study_field' => Study_field::findOrFail($id)]);
    }
    public function storeStudyField(StoreStudyFieldRequest $request){
        $destinationPath = 'public/images/study_field_images';
        $sf = new Study_field;
        $sf->title=$request->title;
        $sf->slug=$sf->make_slug($request->title);
        $sf->content=$request->content;
        if($request->hasFile('icon')) {                  
                $filename = time().$request->icon->getClientOriginalName();
                $sf->icon_name=$filename;
                $request->icon->move('public/images/study_field_icons', $filename);
        }
        $sf->save();
        if($request->hasFile('images')) {
            
            foreach($request->images as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $image->move($destinationPath, $filename);
                $image = new Study_field_image;
                
                $image->study_field_id=$sf->id;
                $image->name=$filename;
                $image->save();
            }
    
        
        }        
                
        
        return back()->withSuccess('Odbor bol úspešne pridaný');
    }
    public function editStudyField(UpdateStudyFieldRequest $request){

        $sf = Study_field::findOrFail($request->id);
        $sf->title = $request->title;
        $sf->content=$request->content;
        $sf->slug=$sf->make_slug($request->title);
        if($request->hasFile('images')) {
            foreach($request->file('images') as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $image->move(public_path('public/images/study_field_images/'), $filename);
                $image = new Study_field_image;
                $image->name=$filename;
                $image->study_field_id=$sf->id;
                $image->save();
            }
        }
        if($request->hasFile('icon')) {
            if(is_file(public_path('public/images/study_field_icons/'.$sf->icon_name))){
                unlink(public_path('public/images/study_field_icons/'.$sf->icon_name));
            }                  
            $filename = time().$request->icon->getClientOriginalName();
            $sf->icon_name=$filename;
            $request->icon->move('public/images/study_field_icons', $filename);
        } 
        $sf->save();
        return back()->withSuccess('Odbor bol úspešne zmenený');
    }
    public function destroyStudyFieldImage(Request $request){
        $sf_image = Study_field_image::findOrFail($request->id);
                 
            if(is_file(public_path('public/images/study_field_images/'.$sf_image->name))){
                unlink(public_path('public/images/study_field_images/'.$sf_image->name));
        }
        $sf_image->delete();
        return back()->withSuccess('Obrazok bol vymazaný');
    }
    public function destroyStudyField(Request $request){       
            $sf = new Study_field;
            $sf = Study_field::findOrFail($request->id);
            
            foreach($sf->study_field_images as $image){           
                $image = 'public/images/study_field_images/'.$image->name;
                if(is_file($image)){
                    unlink($image);
                }
            
            }
            $iconPath = 'public/images/study_field_icons/'.$sf->icon_name;
            if(is_file($iconPath)){
                unlink($iconPath);
            }
            $sf->delete();
    
            return back()->withSuccess('Odbor bol úspešne vymazaný');
    }
}
