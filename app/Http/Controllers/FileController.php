<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreFileSectionRequest;
use App\Http\Requests\StoreFileRequest;
use App\File;
use PhpOffice\PhpWord\Settings;
use App\Filesection;
class FileController extends Controller
{


    public function index()
    {

        $sections = Filesection::get();
        return view('admin.admin-files', ['sections' => $sections]);
    }

    public function show()
    {

        $sections = Filesection::orderBy('sectiontitle', 'asc')->get();
        return view('materialy', ['sections' => $sections]);
    }

    public function showAddSectionFile()
    { 
        return view('admin.add-fileSection');
    }
    public function showAddFile()
    {
        $sections = Filesection::get();
        return view('admin.add-file', ['sections' => $sections]);
    }
    public function showSectionFile()
    {
        $sections = Filesection::paginate(15);
        return view('admin.admin-sectionFiles', ['sections' => $sections]);
    }

    public function storeSectionFile(StoreFileSectionRequest $request)
    {
        $fileSection = new Filesection;
        $fileSection->sectiontitle = $request->sectiontitle;
        $fileSection->save();
        return back()->withSuccess('Sekcia bola úspešne pridana');
    }
    public function showEditSectionFile($id)
    {
        $section = Filesection::findOrFail($id);
        return view('admin.add-fileSection', ['section' => $section]);
    }

    public function editSectionFile(Request $request){
        Filesection::where('id', $request->id)->update(['sectiontitle' => $request->sectiontitle]);
        return back()->withSuccess('Sekcia bola úspešne zmenená');
    }

    public function destroySectionFile(Request $request){
        $section = new FileSection;
        $section = Filesection::findOrFail($request->id);
        
        if(isset($section->files)){
            foreach($section->files as $file){           
                $myFile = 'public/files/'.$file->title;
                if(is_file($myFile)) unlink($myFile);
            }
            
        $section->delete();
        }

        return back()->withSuccess('Sekcia bola úspešne vymazaná');
    }
    public function destroyFile(Request $request){
        $file = new File;
        $file = File::findOrFail($request->id);
        $myFile = 'public/files/'.$file->title;
        if(is_file($myFile)){
            unlink($myFile);
        }
        $file->delete();
       
        return back()->withSuccess('Súbor bol úspešne vymazaný');
    }

    public function storeFile(StoreFileRequest $request){
        $destinationPath = 'public/files';
        $file = new File;
        $file->name = $request->name;
        $file->title = $file->make_file_slug(pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME)).'.'.$request->file->getClientOriginalExtension();
        $file->ext = $request->file->getClientOriginalExtension();
        $file->size = $request->file->getSize();
        $file->filesection_id = $request->section;
        $request->file('file')->move($destinationPath, $file->title);
        $file->save();      
        return back()->withSuccess('Súbor bol úspešne pridaný');
    }
    
    
}

