<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StorePageRequest;
use Illuminate\Http\Request;
use App\Page;
use App\Page_image;
use App\Static_menu_item;
use App\Page_link;

class PageController extends Controller
{
    public function index($slug){
        return view('page', ['page'=>Page::where('slug','=',$slug)->firstOrFail()]);
    }
    public function showAddPage(){
        
        return view('admin.add-page', ['static_menu_items' => Static_menu_item::get()->all()]);
    }
    public function showEditPage($id){
        return view('admin.add-page', ['page' => Page::findOrFail($id),'static_menu_items' => Static_menu_item::get()->all()]);
    }
    public function showAllPages(){
        return view('admin.admin-pages', ['static_pages' => Static_menu_item::get()]);
    }
    public function storePage(StorePageRequest $request){
        $destinationPath = public_path('public/images/page_images');
        $page = new Page;
        $page->title=$request->pageTitle;
        $page->slug=$page->make_slug($request->pageTitle);
        $page->content=$request->content;
        $page->static_menu_item_id = $request->static_menu_item;
        $page->save();
        if($request->hasFile('images')) {
            
            foreach($request->images as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $image->move($destinationPath, $filename);
                $image = new Page_image;
                
                $image->page_id=$page->id;
                $image->name=$filename;
                $image->save();
            }      
        }
        if ($request->url){
            foreach ($request->url as $key => $url) {   
                $link = new Page_link;
                $link->title=$request->title[$key];
                $link->url=$url;
                $link->page_id = $page->id; 
                $link->save();
            }
        }
        if ($request->hasfile('files')){
            foreach ($request->file('files') as $key => $file) {     
                $link = new Page_link;
                $link->title=$request->titlePdf[$key];
                $filename=time().$file->getClientOriginalName();
                $file->move(public_path('public/page-files/'), $filename);
                $link->url='/public/page-files/'.$filename;
                $link->page_id = $page->id; 
                $link->save();
            }
        }                  
        return back()->withSuccess('Stránka bola úspešne pridaná');
    }
    public function destroyPageImage(Request $request){
        
        $page_image = Page_image::findOrFail($request->id);
                 
            if(is_file(public_path('public/images/page_images/'.$page_image->name))){
                unlink(public_path('public/images/page_images/'.$page_image->name));
                $page_image->delete();
        }
        return back()->withSuccess('Obrazok bol vymazaný');
    }

    public function destroyPageLink(Request $request){
        $page_link = Page_link::findOrFail($request->id);      
        if(is_file(public_path(parse_url($page_link->url, PHP_URL_PATH)))){
            unlink(public_path(parse_url($page_link->url, PHP_URL_PATH)));
        }
        $page_link->delete();
        return back()->withSuccess('Odkaz bol úspešne vymazaný');
    }

    public function destroyPage(Request $request){
        
        $page = Page::findOrFail($request->id);
        
        foreach($page->page_images as $image){           
            $image = public_path('public/images/page_images/'.$image->name);
            if(is_file($image)){
                unlink($image);
            }
        }
        foreach($page->page_links as $link){           
            $link = public_path(parse_url($link->url, PHP_URL_PATH));
            if(is_file($link)){
                unlink($link);
            }
        }
        
        $page->delete();

        return back()->withSuccess('Stránka bola úspešne vymazaná');
    }

    public function editPage(StorePageRequest $request){
        $destinationPath = public_path('public/images/page_images');
        $page = Page::findOrFail($request->id);
        $page->title = $request->pageTitle;
        $page->content=$request->content;
        $page->static_menu_item_id=$request->static_menu_item;
        if($request->hasFile('images')) {
            foreach($request->file('images') as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $image->move($destinationPath, $filename);
                $image = new Page_image;
                $image->name=$filename;
                $image->page_id=$page->id;
                $image->save();
            }
        } 
        $page->save();

        if ($request->url){
            foreach ($request->url as $key => $url) {       
                $link = new Page_link;
                $link->title=$request->title[$key];
                $link->url=$url;
                $link->page_id = $page->id; 
                $link->save();
            }
        }
        if ($request->hasfile('files')){
            foreach ($request->file('files') as $key => $file) {       
                $link = new Page_link;
                $link->title=$request->titlePdf[$key];
                $filename=time().$file->getClientOriginalName();
                $file->move(public_path('public/page-files/'), $filename);
                $link->url='public/page-files/'.$filename;
                $link->page_id = $page->id; 
                $link->save();
            }
        }
        return back()->withSuccess('Stránka bola úspešne zmenená');
    }
    public function file(Request $request){
        
        if(is_file(public_path($request->link))) return response()->file(public_path($request->link));
        return redirect($request->link);
    }

}
