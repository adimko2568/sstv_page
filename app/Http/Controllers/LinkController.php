<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLinkRequest;
use App\Http\Requests\StoreLinkSectionRequest;
use App\Linksection;
use App\Link;
use App\Page_link;
class LinkController extends Controller
{
    public function show()
    {
        $sections = Linksection::get();
        return view('odkazy', ['sections' => $sections]);
    }

    public function showAddLinkSection(){
        return view('admin.add-sectionLink');
    }

    public function showAddLink(){
        return view ('admin.add-link', ['linkSections' => Linksection::get()->all()]);
    }
    public function showLinks(){
        return view('admin.admin-links', ['linkSections' => Linksection::get()->all()]);
    }
    public function showLinkSections(){
        return view('admin.admin-linkSections', ['sections' => Linksection::paginate(15)]);
    }

    public function storeSectionLink(StoreLinkSectionRequest $request){
        $section = new Linksection;
        $section->title = $request->title;
        $section->save();
        return back()->withSuccess('Sekcia bola úspešne pridaná');
    }

    public function storeLink(StoreLinkRequest $request){
        $link = new Link;
        $link->title = $request->title;
        if($request->url!=null) $link->url =$request->url;
        else {
            if($request->hasFile('file')) {
                $filename=time().$request->file('file')->getClientOriginalName();
                $request->file('file')->move(public_path('public/files/link-pdf/'), $filename);
                $link->url = 'public/files/link-pdf/'.$filename;
            }
        }
        $link->linksection_id = $request->linksection_id;
        $link->save();
        return back()->withSuccess('Odkaz bol úspešne pridaný');
    }

    public function destroyLink(Request $request){
        $link = Link::findOrFail($request->id);
        if(is_file(public_path(parse_url($link->url, PHP_URL_PATH)))) unlink(public_path(parse_url($link->url, PHP_URL_PATH)));
        $link->delete();
        return back()->withSuccess('Odkaz bol úspešne vymazaný');
    }

    public function showEditLinkSection($id){
        return view ('admin.add-sectionLink', ['section' => Linksection::findOrFail($id)]);
    }

    public function editLinkSection(Request $request){
        $section = Linksection::findOrFail($request->id);
        $section->title = $request->title;
        $section->save();
        return back()->withSuccess('Sekcia bola úspešne upravená');
    }

    public function destroyLinkSection(Request $request){
        $section = Linksection::findOrFail($request->id);
        $section->delete();
        return back()->withSuccess('Sekcia bola úspešne odstranená');
    }

    public function storePageLink(Request $request){
        $link = new Page_link;
        $link->title = $request->title;
        if($request->url!=null) $link->url =$request->url;
        else {
            if($request->hasFile('file')) {
                $filename=time().$request->file('file')->getClientOriginalName();
                $request->file('file')->move(public_path('public/files/page-link-pdf/'), $filename);
                $link->url = request()->getHttpHost().'/public/files/page-link-pdf/'.$filename;
            }
        }
        $link->linksection_id = $request->linksection_id;
        $link->save();
        return back()->withSuccess('Odkaz bol úspešne pridaný');
    }
}
