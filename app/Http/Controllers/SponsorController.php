<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSponsorRequest;
use App\Sponsor;
class SponsorController extends Controller
{

    public function showAllSponsors(){
        return view('admin.admin-sponsors', ['sponsors' => Sponsor::paginate(15)]);
    }
    public function showAddSponsor(){
        return view('admin.add-sponsor');
    }
    public function storeSponsor(StoreSponsorRequest $request){
        $sponsor = new Sponsor;
        $sponsor->sponsorUrl=$request->sponsorUrl;
        if($request->hasFile('img')) {
            $filename=time().$request->file('img')->getClientOriginalName();
            $request->file('img')->move(public_path('public/images/sponsorImages'), $filename);
            $sponsor->img = $filename;
        }
        $sponsor->save();
        return back()->withSuccess('Sponzor bol úspešne pridaný');
    }
    public function destroySponsor(Request $request){
        $sponsor = new Sponsor;
        $sponsor = Sponsor::findOrFail($request->id);
        
                
        $image = public_path('public/images/sponsorImages/'.$sponsor->img);
            
        if(is_file($image)){
            unlink($image);
        }
        
        $sponsor->delete();

        return back()->withSuccess('Sponzor bol úspešne vymazaný');
    }

}
