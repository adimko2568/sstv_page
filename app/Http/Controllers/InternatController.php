<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Internat_info;
use App\Internat_image;
class InternatController extends Controller
{
    public function index(){
        return view('internat', ['internat' => Internat_info::findOrFail('1')]);
    }
    public function showEditInternat(){
        return view('update-internat', ['internat' => Internat_info::findOrFail('1')]);
    }
    public function destroyImage(Request $request){
        $internat_image = Internat_image::findOrFail($request->id);
                 
            if(is_file('public/images/internatImages/'.$internat_image->name)){
                unlink('public/images/internatImages/'.$internat_image->name);
                $internat_image->delete();
        }
        return back()->withSuccess('Obrazok bol vymazaný');
    }
    public function editInternat(Request $request){
        $internat = Internat_info::findOrFail('1');
        $internat->content = $request->content;
        $internat->food = $request->food;
        $internat->contact = $request->contact;
        $internat->address = $request->address;
        if($request->hasFile('images')) {
            foreach($request->file('images') as $image) {
                
                $filename = time().$image->getClientOriginalName();
                $image->move(public_path('public/images/internatImages/'), $filename);
                $image = new Internat_image;
                $image->name=$filename;
                $image->internat_info_id=$internat->id;
                $image->save();
            }
        } 
        $internat->save();
        return back()->withSuccess('Internát bol úspešne zmenený');
    }
}
