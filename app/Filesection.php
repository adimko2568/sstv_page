<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Filesection extends Model
{
    protected $fillable = [
        'sectiontitle',
    ];

    public function files()
    {
        return $this->hasMany('App\File')->orderBy('title','asc');
    }
}   
