<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class File extends Model
{
    protected $fillable = [
        'title','name','ext','size','filesection_id',
    ];

    public function make_file_slug($name){
        return Str::slug($name);
    }
}
