<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Static_menu_item;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app', function ($view) {
            $myvar = Static_menu_item::get();
            $view->with('menu_items', $myvar);
        });
        // view()->composer('layout.app', function($view) {
        //     $myvar = static_menu_item::get();
        //     $view->with('menu_items', $myvar);
        //     dd($mywar);
        //     $view->with('data', array('menu_items' => $myvar));
        // });
    }
}
