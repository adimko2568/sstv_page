<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Post_image extends Model
{
    protected $fillable = [
        'name', 'ext', 'size',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}

