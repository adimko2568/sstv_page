<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Study_field extends Model
{
    protected $fillable = [
        'title', 'content', 'icon_path','slug',
    ];

    public function study_field_images()
    {
        return $this->hasMany(Study_field_image::class);
    }
    public function make_slug($name){
        return Str::slug($name);
    }
}
