<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Post extends Model
{
    protected $fillable = [
        'title', 'content', 'title_image','created_at',
    ];

    public function images()
    {
        return $this->hasMany(Post_image::class);
    }

    public function short_text(){
         return Str::limit($this->content, 550, '... <a href="/prispevok/'.$this->id.'">čítaj ďalej</a>');
    }

}
