<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Internat_info extends Model
{
    protected $fillable = [
        'content','food','contact','address','created_at','updated_at',
    ];
    public function internatImages()
    {
        return $this->hasMany(Internat_image::class);
    }
}
