<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Static_menu_item extends Model
{
    public function pages()
    {
        return $this->hasMany('App\Page');
    }
}
