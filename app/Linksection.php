<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Linksection extends Model
{
    public function links()
    {
        return $this->hasMany('App\Link')->orderBy('title','asc');
    }
}
