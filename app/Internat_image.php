<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Internat_image extends Model
{
    protected $fillable = [
        'name','internatInfo_id','created_at','updated_at',
    ];
}
