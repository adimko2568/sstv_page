<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//USERS ROUTES
Route::get('/prispevok/{id}', 'PostController@show_one');
Route::get('/', 'HomeController@index');
Route::get('/kontakt', function () {
  return view('contactPage');
});
Route::get('/internat', function () {
  return view('internatPage');
});
Route::get('/ubytovna', function () {
  return view('ubytovna');
});
Route::get('/materialy', function () {
  return view('materialy');
});
Route::get('/page/{slug}', 'PageController@index');
Route::get('/odkazy', function () {
  return view('odkazy');
});
Route::get('/prispevky', 'PostController@show_all');
Route::get('/materialy', 'FileController@show');
Route::get('/odkazy', 'LinkController@show');
Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = public_path("public/files/".$filename);
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9-_.]+');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/odbor/{field}', 'Study_fieldController@showField');
Route::get('/odkaz', 'PageController@file');

//ADMIN ROUTES-------------------------------------------------------------------------

Route::group(['middleware' => ['isModerator']], function () {
  //
  Route::get('/admin', 'PostController@index')->name('post');

  //INTERNAT ROUTES
  Route::get('/edit-internat', 'InternatController@showEditInternat');
  Route::post('/destroyInternatImage', 'InternatController@destroyImage')->name('destroyInternatImage');
  Route::post('/editInternat', 'InternatController@editInternat');


  //POST ROUTES
  Route::post('/destroyPost', 'PostController@destroyPost')->name('destroyPost');
  Route::post('/editPost', 'PostController@editPost')->name('editPost');
  Route::post('/destroyPostImage', 'PostController@destroyImage')->name('destroyPostImage');
  Route::get('/admin/posts', 'PostController@index');
  Route::get('/update-post/{id}', 'PostController@showEditPost')->name('showEditPost');
  Route::get('/add-post', 'PostController@showAddPost')->name('showAddPost');
  Route::post('/storePost', 'PostController@storePost')->name('storePost');

  //FILES ROUTES
  Route::get('/admin/files', 'FileController@index');
  Route::get('/admin/sectionFiles', 'FileController@showSectionFile')->name('showSectionFile');
  Route::post('/editSectionFile', 'FileController@editSectionFile')->name('editSectionFile');
  Route::post('/destroySectionFile', 'FileController@destroySectionFile')->name('destroySectionFile');
  Route::post('/destroyFile', 'FileController@destroyFile')->name('destroyFile');
  Route::get('/update-sectionfile/{id}', 'FileController@showEditSectionFile')->name('showEditSectionFile');
  Route::get('/add-fileSection', 'FileController@showAddSectionFile')->name('showAddSectionFile');
  Route::get('/add-file', 'FileController@showAddFile')->name('showAddFile');
  Route::post('/storeSectionFile', 'FileController@storeSectionFile')->name('storeSectionFile');
  Route::post('/storeFile', 'FileController@storeFile')->name('storeFile');

  //STUDY FIELD ROUTES
  Route::get('/add-study_field', 'Study_fieldController@showAddStudyField');
  Route::get('/admin/study_fields', 'Study_fieldController@showAll');
  Route::post('/destroyStudyField', 'Study_fieldController@destroyStudyField')->name('destroystudyField');
  Route::post('/storeStudyField', 'Study_fieldController@storeStudyField')->name('storeStudyField');
  Route::get('/update-studyField/{id}', 'Study_fieldController@showEditStudyField');
  Route::post('/editStudyField', 'Study_fieldController@editStudyField')->name('editStudyField');
  Route::post('/destroyStudyFieldImage', 'Study_fieldController@destroyStudyFieldImage')->name('destroyStudyFieldImage');



  //SPONSOR ROUTES
  Route::get('/admin-sponsors', 'SponsorController@showAllSponsors');
  Route::get('/add-sponsor', 'SponsorController@showAddSponsor');
  Route::post('/storeSponsor', 'SponsorController@storeSponsor')->name('storeSponsor');
  Route::post('/destroySponsor', 'SponsorController@destroySponsor')->name('destroySponsor');
  
  //LINKS ROUTES
  Route::get('/admin/linkSections', 'LinkController@showLinkSections');
  Route::get('/admin/links', 'LinkController@showLinks');
  Route::get('/add-linkSection', 'LinkController@showAddLinkSection');
  Route::get('/add-link', 'LinkController@showAddLink');
  Route::get('/update-linkSection/{id}', 'LinkController@showEditLinkSection');
  Route::post('/storeSectionLink', 'LinkController@storeSectionLink')->name('storeSectionLink');
  Route::post('/storeLink', 'LinkController@storeLink')->name('storeLink');
  Route::post('/destroyLink', 'LinkController@destroyLink')->name('destroyLink');
  Route::post('/editLinkSection', 'LinkController@editLinkSection')->name('editLinkSection');
  Route::post('/destroyLinkSection', 'LinkController@destroyLinkSection')->name('destroyLinkSection');
  //PAGES ROUTES
  Route::get('/admin-pages', 'PageController@showAllPages');
  Route::get('/add-page', 'PageController@showAddPage');
  Route::get('/update-page/{id}', 'PageController@showEditPage');
  Route::post('/storePage', 'PageController@storePage')->name('storePage');
  Route::post('/destroyPageImage', 'PageController@destroyPageImage')->name('destroyPageImage');
  Route::post('/destroyPageLink', 'PageController@destroyPageLink')->name('destroyPageLink');
  Route::post('/destroyPage', 'PageController@destroyPage')->name('destroyPage');
  Route::post('/editPage', 'PageController@editPage')->name('editPage');
  //USER ROUTES
  Route::get('/admin/changeName', 'UserController@showChangeName');
  Route::post('/changeName', 'UserController@changeName')->name('changeName');
  Route::get('/admin/changePassword', 'UserController@showChangePassword');
  Route::post('/changePassword', 'UserController@changePassword')->name('changePassword');
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
  \UniSharp\LaravelFilemanager\Lfm::routes();
});
// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
  ]);
  Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
  ]);
  Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
  ]);
  
  Route::group(['middleware' => ['isAdmin']], function () {
  Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
  ]);
  Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
  ]);
  Route::get('/admin/users', 'UserController@showUsers');
  Route::post('/destroyUser', 'UserController@destroyUser')->name('destroyUser');
  });




