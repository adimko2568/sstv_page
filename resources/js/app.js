

try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

import createLinkInputs from './add-page';
import 'lightbox2/dist/js/lightbox-plus-jquery.min';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'hamburgers/dist/hamburgers.min.css';
require('bootstrap');
require('owl.carousel');

window.Vue = require('vue');

Vue.component('hamburger-menu', require('./components/HamburgerMenu.vue').default);

const app = new Vue({
    el: '#app',
});

// LINKS FORM on ADD-PAGE
$('#addLinks').on('submit',function (e) {
    e.preventDefault();
    let title = $(this).find('input[name="link-title"]');
    let url = $(this).find('input[name="link-url"]');
    let file = $(this).find('input[id="link-file"]'); 
    
    let params = {
        title: title.val(),
        content: {
            type: 'url',
            path: url.val(),
            isFile: false
        }
    }

    if(file.val()) {
        params.title = title.val();
        params.content.type = 'file';
        params.content.path = file.val();
        params.content.isFile = true;
    }
    createLinkInputs(params);

    $('#addLinkModal').modal('hide');

    file.val('');
    title.val('');
    url.val('');
})

// alerts
if($('.alert')) {
    setTimeout(() => {
        $('.alert').fadeOut(5000);
    }, 5000);
}

// footer year
$('#year').html(new Date().getFullYear());

// files table 
$('.files-section-name').on('click', function () {
    
    if ($(this).hasClass('section-expanded')){
        $(this).removeClass('section-expanded');
        $(this).find('.hide').css( "display", "none" );
        $(this).find('.hide').siblings().css( "display", "block" );
        $(this).siblings().css( "display", "none" );
    }
    else {
        $(this).addClass('section-expanded');
        $(this).find('.show').css( "display", "none" );
        $(this).find('.show').siblings().css( "display", "block" );
        $(this).siblings().css( "display", "block" );
    }
    
})

window.onload = (event) => {

    $('#nav-icon').click(function(){
        if ($(this).hasClass('nav-icon-opened')){
            $('.hamburger-menu').removeClass('slide-in-right');
            $(this).toggleClass('nav-icon-opened');
            $('.hamburger-menu').toggleClass('slide-out-right');
        }
        else {
            $('.hamburger-menu').removeClass('slide-out-right');
            $(this).toggleClass('nav-icon-opened');
            $('.hamburger-menu').css('display','flex');
            $('.hamburger-menu').toggleClass('slide-in-right');
        }
    });

    $('.burger-item').click(function () {  
        $(this).toggleClass('opened');
        $(this).find('.burger-item-dropdown').toggleClass('opened');
        $(this).siblings().find('.burger-item-dropdown').removeClass('opened');
        $(this).siblings().removeClass('opened');
        
    })

    if(window.pageYOffset == 0) {
        $(window).one( "scroll", function() {
            if ($('#fields').length) {
                const top = $('#fields').offset().top;

                window.scrollTo({
                    top: top - 200,
                    behavior: 'smooth',
                  });
            }
        });
    }


    // articles carousel
    window.onscroll = () => {
        setTimeout(() => {
            if (window.pageYOffset > 20) {
                $('nav').addClass('nav-bg');
                $('nav ul li a img').removeClass('scale-up-center');
                $('nav ul li a img').addClass('scale-down-center');
            }
            else {
                $('nav').removeClass('nav-bg');
                $('nav ul li a img').removeClass('scale-down-center');
                $('nav ul li a img').addClass('scale-up-center');
            }
        }, 150);
    }

    $('.owl-carousel').owlCarousel({
        loop:false,
        margin: 30,
        autoWidth: false,
        nav: true,
        navText: ['<div class="prev">','<div class="next">'],
        mouseDrag: false,
        freeDrag: false,
        smartSpeed: 600,
        responsive : {
            1550 : {
                items: 3
            },
            1000: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    })
    $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this)
        var id = button.data('id');
        modal.find('#delete-form input[name="id"]').val(id);
        console.log($('#delete-form input[name="id"]'));
    })

    ScrollReveal().reveal('.fields-of-study', {
        duration: 1500,
        distance: '50px',
        origin: 'left',
        mobile: false,
        viewFactor: 1
    });

    ScrollReveal().reveal('.articles-wrapper', {
        duration: 1500,
        distance: '50px',
        origin: 'bottom',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.links a:nth-child(1)', {
        duration: 1500,
        distance: '50px',
        origin: 'left',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.links a:nth-child(2)', {
        duration: 1500,
        delay:300,
        distance: '50px',
        origin: 'left',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.links a:nth-child(3)', {
        duration: 1500,
        delay:500,
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.links a:nth-child(4)', {
        duration: 1500,
        delay:300,
        distance: '50px',
        origin: 'right',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.links a:nth-child(5)', {
        duration: 1500,
        distance: '50px',
        origin: 'right',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.counters div:nth-child(1)', {
        duration: 1500,
        distance: '150px',
        origin: 'top',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.counters div:nth-child(2)', {
        duration: 1500,
        delay: 300,
        distance: '100px',
        origin: 'top',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.counters div:nth-child(3)', {
        duration: 1500,
        delay: 400,
        distance: '50px',
        origin: 'top',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.why-us .article .article-texts', {
        duration: 1000,
        distance: '200px',
        origin: 'left',
        mobile: false,
        viewFactor: .5
    });
    
    ScrollReveal().reveal('.why-us .article .article-img', {
        duration: 1000,
        distance: '200px',
        origin: 'right',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.applications h1', {
        duration: 1000,
        distance: '100px',
        origin: 'top',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.applications a', {
        duration: 1000,
        distance: '100px',
        origin: 'bottom',
        mobile: false,
        viewFactor: .5
    });

    ScrollReveal().reveal('.quote', {
        duration: 1000,
        distance: '150px',
        origin: 'bottom',
        mobile: false,
        viewFactor: .5
    });
};


