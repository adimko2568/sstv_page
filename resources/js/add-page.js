try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

export default function createLinkInputs(el) {
    let html,x=null, btn = null;
    let length = $('.link-group').length + 1
    
    if(!el.content.isFile) {
        html = `
    <div class="form-group d-flex link-group group`+length+`">
        <input class="form-control mr-3" readonly type='text' name='title[]' value='`+el.title+`'>
        <input class="form-control mr-3" readonly type='text' name='`+el.content.type+`[]' value='`+el.content.path+`'>
        <button type="button" class="remove-link-bttn btn btn-primary">x</button>
    </div>`
    }
    else {
        html = `
    <div class="form-group d-flex link-group group`+length+`">
        <input class="form-control mr-3" readonly type='text' name='titlePdf[]' value='`+el.title+`'>`
        btn = `
        <button type="button" class="remove-link-bttn btn btn-primary">x</button>
    </div>`
        x = $('#addLinks').find('input[name="files[]"]').clone();
        x.attr('id','');
    }
    
    
    $('#addPageForm').append(html);
    $('.group'+length).append(x, btn);
    $('.remove-link-bttn').on('click',function () { 
        $(this).parent().remove();
    })
}