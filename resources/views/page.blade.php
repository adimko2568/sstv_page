@extends('layouts.app')

@section('header')
    <h2>{{$page->title}}</h2>
@endsection



@section('content')
    <div class="page-container">
        <div class="content">
            {!!$page->content!!}
        </div>
        @if (count($page->page_links) > 0)
            @foreach($page->page_links as $link)
            <form action="/odkaz" method="GET">
                <input type="hidden" name="link" value="{{$link->url}}">
                <button class="btn btn-link" type="submit">{{$link->title}}</button>
                {{csrf_field()}}
            </form>
             
            @endforeach
        @endif
        @if (count($page->page_images) > 0)
        <div class="gallery-wrapper">
            <h1>Galéria</h1>
            <div class="gallery-container">
                @foreach($page->page_images as $image)
                    <div class="gallery-photo">
                        <a href="/public/images/page_images/{{$image->name}}" data-lightbox="{{$page->title}}" data-title="{{$page->title}}">
                            <img src="/public/images/page_images/{{$image->name}}" alt="">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
@endsection
