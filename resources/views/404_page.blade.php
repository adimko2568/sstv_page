@extends('layouts.login')

@section('content')
    <div class="wrapper-404">
        <div class="top-container">
            <img src="{{ asset('assets/logo.png') }}" alt="Spojená škola logo">
            <div class="texts">
                <h1>4<span>0</span>4</h1>
                <h2>Page not found</h2>
            </div>
        </div>
        <div class="bottom-container">
            <a href="/">
                <h1>Vráť sa na stránku školy</h1>
            </a>
        </div>
    </div>
@endsection