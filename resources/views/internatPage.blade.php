@extends('layouts.app')

@section('header')
    <h1>Internát</h1>
    <p>"Máš to ďaleko? Nevadí máme intrák!"</p>
@endsection

@section('content')
    <div class="college-wrapper">
        <div class="article">
            <div class="article-texts">
                <h1>Školský internát</h1>
                <p>Nástup žiakov ubytovaných v školskom internáte v školskom roku 2018/2019 je v nedeľu 02.09.2018 od 16.00 hod.Tel. kontakt – 043 532071, 0908 235 724</p>
                <p>Školský internát je súčasťou uzavretého komplexu Spojenej školy Tvrdošín. Nachádza sa v centre sídliska Medvedzie, oproti hlavnej komunikačnej cesty a autobusových zástaviek. Vo vzdialenosti 150 metrov od internátu sa nachádza nákupné centrum TESCO a 350 metrov LIDL.</p>
                <p>V súčasnosti školský internát poskytuje ubytovanie žiakom zo štyroch stredných škôl a to zo Spojenej školy Tvrdošín Strednej odbornej školy lesníckej Tvrdošín, Gymnázia Tvrdošín a Gymnázia Martina Hattalu Trstená.</p>
                <p>Školský internát je výchovno-vzdelávacie zariadenie, ktoré zabezpečuje žiakom stredných škôl výchovno-vzdelávaciu starostlivosť v čase mimo vyučovania, stravovanie a ubytovanie.</p>
            </div>
            <div class="article-img">
                <img src="{{ asset('assets/internat-photo1.png') }}" alt="">
            </div>
        </div>
        <div class="second-college-article article">
            <div class="article-img">
                <img src="{{ asset('assets/internat-photo2.png') }}" alt="">
            </div>
            <div class="article-texts">
                <h2>Personálne obsadenie internátu:</h2>
                <p>Výchovno-vzdelávaciu činnosť zabezpečujú traja vychovávatelia. Ubytovanie v školskom internáte:</p>
                <p>Ubytovanie je realizované v samostatnej 7. poschodovej budove s výťahom, ktorá je súčasťou komplexu Spojenej školy Tvrdošín, s prepojením na školskú jedáleň, telocvičňu, posilňovňu, multifunkčné ihrisko.</p>
                <ul>
                    <li>cena za lôžko je 20,00 €/mesiac – platba na č. účtu: SK33 8180 0000 0070 0048 2697</li>
                    <li>bunkový systém s malou chodbičkou so zabudovanými chodbovými skriňami</li>
                    <li>počet žiakov na izbách je 2+2, 2+3</li>
                    <li>sociálne vybavenie: každá bunka má vlastné WC, sprcha a dve umývadlá pre každé dve izby – (to je jedna bunka)</li>
                    <li>na izbe je k dispozícii pre každého žiaka válenda, písací stolík so stoličkou, polička, skrinka na osobné veci</li>
                </ul>
                
                <h2>V internáte je k dispozícii:</h2>
                <ul>
                    <li>kuchynka s príslušenstvom (mikrovlnná rúra, rýchlo varné kanvice, chladnička, elektrický varič, elektrická rúra a kuchynské vybavenie)</li>
                    <li>spoločenská miestnosť a klubovňa na každom poschodí s vybavením TV/SAT</li>
                    <li>klubovňa a študovňa, knižnica s čitárňou</li>
                    <li>počítačová miestnosť s PC s pripojením na internet, možnosť tlačenia dokumentov</li>
                    <li>posilňovňa (možnosť pravidelne rekondične cvičiť)</li>
                    <li>telocvičňa (možnosť hrať kolektívne športy – florbal, futbal, volejbal, basketbal, hádzanú, nohejbal, robiť kultúrno športové podujatia …)</li>
                    <li>stolnotenisový stôl a spoločenské hry</li>
                    <li>miestnosť na prezentáciu žiackej tvorby</li>
                </ul>
                <p>Stravovanie je poskytované vo vlastnej školskej jedálni s kuchyňou. Žiaci majú možnosť výberu z dvoch jedál prostredníctvom čipových kariet cez PC</p>
            </div>
        </div>
    </div>

    <div class="college-info-wrapper">
        <div class="college-info-item">
            <div class="college-info-item-heading">
                <div class="college-info-item-icon">
                    <img src="{{ asset('assets/restaurant-icon.png') }}" alt="Location icon">
                </div>
                <h1>Stravovanie</h1>
            </div>
            <div class="college-info-item-texts item-texts-flex">
                <div class="college-info-item-texts-section">
                    <p>Raňajky</p>
                    <p>Obed</p>
                    <p>Večera</p>
                    <p>Celodenný stravný lístok</p>
                </div>
                <div class="college-info-item-texts-section">
                    <p>1,19 €</p>
                    <p>1,46 €</p>
                    <p>1,35 €</p>
                    <p>4,00 €</p>
                </div>
                <div class="college-info-item-texts-section">
                    <p>od 06.30 h do 07.15h</p>
                    <p>od 11.15 h do 14.30h</p>
                    <p>od 18.00 h do 18.30h</p>
                </div>
            </div>
        </div>
        <div class="college-info-item">
            <div class="college-info-item-heading">
                <div class="college-info-item-icon">
                    <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                </div>
                <h1>Kontakt</h1>
            </div>
            <div class="college-info-item-texts">
                <p>043/5831271 – vychovávatelia na internáte</p>
                <p>043/5831222 – ekonómka školského internátu</p>
                <p>043/5831215 – vrátnica školy</p>
                <p>043/5831210 – sekretariát školy</p>
            </div>

            <div class="college-info-item-texts align-center">
                <p>internatikts@gmail.com</p>
                <p>spstv@spstv.edu.sk</p>
                <p>zakova@spstv.edu.sk</p>
            </div>
        </div>
        <div class="college-info-item">
            <div class="college-info-item-heading">
                <div class="college-info-item-icon">
                    <img src="{{ asset('assets/location-icon.png') }}" alt="Location icon">
                </div>
                <h1>Adresa</h1>
            </div>
            <div class="college-info-item-texts">
                <p>Školský internát pri Spojenej škole Tvrdošín</p>
                <p>Medvedzie 133/1</p>
                <p>027 44  Tvrdošín</p>
            </div>
        </div>
    </div>

    <div class="college-gallery-wrapper">
        <h1>Galéria</h1>
        <div class="college-gallery-container">
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/1-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/1.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/2-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/2.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/3-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/3.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/4-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/4.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/5-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/5.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/6-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/6.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/7-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/7.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/8-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/8.png') }}" alt="">
                </a>
            </div>
            <div class="college-gallery-photo">
                <a href="{{ asset('assets/internat-photos/9-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                    <img src="{{ asset('assets/internat-photos/9.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
@endsection
