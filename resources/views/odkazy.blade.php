@extends('layouts.app')

@section('header')
    <h1>Odkazy</h1>
    <p>"Všetko čo potrebuješ"</p>
@endsection

@section('content')
    <div class="files-page-wrapper">
        <div class="files-wrapper">
            @foreach ($sections as $section)
            <div class="files-section">
                <div class="files-section-name">
                    <h1>{{$section->title}}</h1>
                    <div class="files-section-buttons">
                        <img class="show" src="{{ asset('assets/show.png') }}" alt="Show">
                        <img class="hide" src="{{ asset('assets/hide.png') }}" alt="Hide">
                    </div>
                </div>
                <div class="files-section-body">
                    <table>
                          <tbody>
                            @foreach ($section->links as $link)
                            <tr>
                                <td>
                                    <form action="/odkaz" method="GET">
                                        <input type="hidden" name="link" value="{{$link->url}}">
                                        <button class="btn btn-link" type="submit">{{$link->title}}</button>
                                        {{csrf_field()}}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                    </table>
                </div>
            </div>
    @endforeach
        </div>
    </div>
@endsection
