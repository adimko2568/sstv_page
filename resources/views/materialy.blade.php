@extends('layouts.app')

@section('header')
    <h1>Materiály</h1>
    <p>"Všetko čo potrebuješ"</p>
@endsection

@section('content')
    <div class="files-page-wrapper">
        <div class="files-wrapper">
            @foreach ($sections as $section)
            <div class="files-section">
                <div class="files-section-name">
                    <h1>{{$section->sectiontitle}}</h1>
                    <div class="files-section-buttons">
                        <img class="show" src="{{ asset('assets/show.png') }}" alt="Show">
                        <img class="hide" src="{{ asset('assets/hide.png') }}" alt="Hide">
                    </div>
                </div>
                <div class="files-section-body">
                    <table>
                        <thead>
                            <tr>
                              <th>Názov dokumentu</th>
                              <th>Veľkosť</th>
                              <th>Stiahnuť</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($section->files as $file)
                            <tr>
                                <td>{{$file->name}}</th>
                                <td>{{round($file->size/1000, 0)}} KB</td>
                                <td class="stiahnut"><a href="/download/{{$file->title}}">Stiahnuť</a></td>
                            </tr>
                            @endforeach
                          </tbody>
                    </table>
                </div>
            </div>
    @endforeach
        </div>
    </div>
@endsection
