@extends('layouts.app')

@section('header')
    <h1>Ubytovňa</h1>
    <p>"Sme lacnejší ako hotel s rôznymi výhodami!"</p>
@endsection

@section('content')
    <div class="hostel-wrapper">
        <div class="article">
            <div class="article-texts">
                <h1>Ubytovňa</h1>
                <p>Izby sú vybavené 3 alebo 4 posteľami. Každá izba má sociálne zariadenie a sprchu.</p>
                <p>Pre každé poschodie je jeden TV prijímač (káblová televízia).</p>
                <p>Pri vchode do ubytovne je parkovací priestor (kapacita pre dva autobusy a šesť áut.</p>
                <p>Na treťom poschodí je kuchynka, kuchynská linka, chladnička, elektrický sporák, mikrovlnka.</p>
            </div>
            <div class="article-img">
                <img src="{{ asset('assets/ubytovna.png') }}" alt="">
            </div>
        </div>
    </div>

    <section class="hostel section-wrapper">
        <div class="number-of-beds">
            <div>
                <div>
                    <h1>Počet lôžok</h1>
                </div>
                <span>74</span>
            </div>
            <div>
                <div>
                    <h1>Počet lôžok</h1>
                    <p>počas letných prázdnin</p>
                </div>
                <span>173</span>
            </div>
        </div>
    </section>

    <div class="hostel-wrapper">
        <div class="hostel-gallery-wrapper">
            <h1>Galéria</h1>
            <div class="college-gallery-container">
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/1-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/1.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/2-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/2.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/3-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/3.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/4-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/4.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/5-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/5.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/6-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/6.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/7-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/7.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/8-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/8.png') }}" alt="">
                    </a>
                </div>
                <div class="college-gallery-photo">
                    <a href="{{ asset('assets/internat-photos/9-lg.jpg') }}" data-lightbox="ubytovna-galeria" data-title="Ubytovňa">
                        <img src="{{ asset('assets/internat-photos/9.png') }}" alt="">
                    </a>
                </div>
            </div>
        </div>

        <div class="hostel-contact contact-section">
            <div class="contact-item">
                <div class="contact-item-icon">
                    <img src="{{ asset('assets/location-icon.png') }}" alt="Location icon">
                </div>
                <div class="contact-item-texts">
                    <p>Školský internát</p>
                    <p>Medvedzie 133/1</p>
                    <p>027 44 Tvrdošín</p>
                </div>
            </div>
            <div class="contact-item">
                <div class="contact-item-icon">
                    <img src="{{ asset('assets/phone-icon.png') }}" alt="Phone icon">
                </div>
                <div class="contact-item-texts">
                    <h2>Eva Žáková</h2>
                    <p>043 5831222</p>
                    <p>0914 134 792</p>
                </div>
            </div>
            <div class="contact-item">
                <div class="contact-item-icon">
                    <img src="{{ asset('assets/mail-icon.png') }}" alt="Email icon">
                </div>
                <div class="contact-item-texts">
                    <p>
                        <a href="mailto:zakova@spstv.edu.sk">zakova@spstv.edu.sk</a>
                    </p>
                </div>
            </div>
        </div>  
    </div>
    
@endsection
