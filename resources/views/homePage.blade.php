@extends('layouts.app')

@section('header')
    <h2>Spojená škola Tvrdošín</h2>
    <h3>"Neučíme sa pre školu, ale pre život."</h3>
@endsection


@section('content')
    <section class="fields-of-study" id="fields">
        <h1>Študijné odbory</h1>
        <div class="fields">
        
            @foreach ($study_fields as $study_field)
                <div>
                    <a href="odbor/{{$study_field->slug}}">
                        <img src="/public/images/study_field_icons/{{$study_field->icon_name}}" alt="">
                    </a>
                    <a href="odbor/{{$study_field->slug}}">
                        <h2>{{$study_field->title}}</h2>
                    </a>
                </div>
            @endforeach
        </div>
    </section>

    <div class="articles-wrapper owl-carousel">
        @foreach($posts as $post)
            <div class="homepage-article">
                <a href="/prispevok/{{$post->id}}">
                    <h1>{{$post->title}}</h1>
                </a>
                <p class="date">{{date('d.m.Y', strtotime($post->created_at))}}</p>
                <div class="article-texts">
                    <p>{!!$post->short_text()!!}</p>
                </div>
                <div class="article-img">
                    <a href="/public/images/postImages/{{$post->title_image}}" data-lightbox="{{$post->id}}" data-title="{{$post->title}}">
                        <img src="/public/images/postImages/{{$post->title_image}}" alt="">
                    </a>
                </div>
                <div class="article-images-all">
                    @foreach($post->images as $image)
                        <a href="/public/images/postImages/{{$image->name}}"  data-lightbox="{{$post->id}}" data-title="{{$post->title}}">fsdaf</a>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>

    <div class="show-all-articles">
        <a href="/prispevky">
            <h1>Zobraziť všetky články...</h1>
        </a>
    </div>

    <section class="links">
        <a href="#" target="_blank">
            <div class="bg-fields">
                <h2>Študijné odbory</h2>
            </div>
        </a>
        <a href="https://sstv.edupage.org/login/?msg=3" target="_blank">
            <div class="bg-edupage">
                <h2>Žiacka knižka</h2>
            </div>
        </a>
        <a href="https://sstv.edupage.org/login/" target="_blank">
            <div class="bg-classbook">
                <h2>Triedna kniha</h2>
            </div>
        </a>
        <a href="https://sstv.edupage.org/timetable/?" target="_blank">
            <div class="bg-schedule">
                <h2>Rozvrh hodín</h2>
            </div>
        </a>
        <a href="https://www.eskoly.sk/" target="_blank">
            <div class="bg-foodlist">
                <h2>Jedálny lístok</h2>
            </div>
        </a>
    </section>
    <section class="counters-wrapper section-wrapper">
        <div class="counters">
            <div>
                <img src="{{ asset('assets/odbory-counter.png') }}" alt="">
                <h2>Počet odborov</h2>
                <h3>5</h3>
            </div>
            <div>
                <img src="{{ asset('assets/ziaci-counter.png') }}" alt="">
                <h2>Počet žiakov</h2>
                <h3>574</h3>
            </div>
            <div>
                <img src="{{ asset('assets/ucitelia-counter.png') }}" alt="">
                <h2>Počet učiteľov</h2>
                <h3>39</h3>
            </div>
        </div>
    </section>
    <section class="why-us">
        <div class="article">
            <div class="article-texts">
                <h1>Prečo študovať u nás?</h1>
                <p>Máme niekoľko jednoduchých dôvodov na to, prečo by ste mali študovať práve u nás! Naša škola je vhodná pre všetkých, ktorí majú záujem o vysokokvalitné štúdium, ktoré ich dokáže posunúť vpred v rôznych oblastiach.</p>
                <p>Štúdium na našej škole je hodnotené certifikátmi za dobrý aj snahu </p>
                <p>Naši študenti sa pohybujú v priestoroch školy, ktoré sú vybavené rôznymi prostriedkami a zariadeniami pre zvýšenie záujmu o štúdium.</p>
                <p>Po ukončení štúdia je absolvent pripravený pokračovať v štúdiu na vysokej škole, prípadne môže využiť praktické vedomosti  v práci.</p>
                <p>Uchádzači o štúdium na našej škole si môžu vybrať z umeleckých a technických odborov.</p>
            </div>
            <div class="article-img">
                <img src="{{ asset('assets/why-us-photo.png') }}" alt="">
            </div>
        </div>
    </section>
    <section class="applications section-wrapper">
        <h1>Chcem študovať!</h1>
        <a href="/page/stipendium">Prihlášky</a>
    </section>
    <section class="quote">
        <img src="{{ asset('assets/quote.png') }}" alt="">
        <h2>“Priemerný učiteľ povie. Dobrý vysvetlí. Skvelý demonštruje. Veľký inšpiruje.”</h2>
    </section>
    
    <section class="sponsors section-wrapper">
        <h1>Sponzori</h1>
        <div id="sponsors-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="sponsors-item">
                @foreach ($sponsors as $key => $sponsor)
                    @if ($key != 0)
                        <a href="{{$sponsor->sponsorUrl}}" target="_blank">
                            <img src="/public/images/sponsorImages/{{$sponsor->img}}" alt="">
                        </a>
                    @endif
                    @if ($key%5 == 0 && $key != 0)
                        </div>
                        </div>
                        <div class="carousel-item">
                        <div class="sponsors-item">
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
