<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}"/> 
    <title>Spojená škola Tvrdošín</title>
    <script src="https://kit.fontawesome.com/3763afafbc.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <script src="{{asset('js/bootstrap.js')}}"></script>
    

</head>
<body>
<div class="container-fluid">

    
       
        <section class="sideMenu">
            <div class="sideMenuHeader">
                <a href="/"><img src="{{asset('assets/logo.png')}}" alt="Logo školy"></a>
            </div>
            <div class="sideMenuInsider">
                
                <ul>
                
                    <li>             
                        <a href="/admin/study_fields" class="btn btn-secondary " >
                        <i class="fas fa-atlas"></i>Študíjne odbory</a>       
                    </li>
                    <li>             
                        <a href="/admin/posts" class="btn btn-secondary " >
                        <i class="fas fa-mail-bulk "></i>Príspevky</a>       
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class=" fas fa-download"></i>Súbory</button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/admin/files">Precházať súbory</a>
                                <a class="dropdown-item" href="/admin/sectionFiles">Prechádzať sekciu</a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class=" fas fa-link"></i>Odkazy</button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/admin/links">Precházať odkazy</a>
                                <a class="dropdown-item" href="/admin/linkSections">Prechádzať sekcie</a>
                            </div>
                        </div>
                    </li>

                    <li>             
                        <a href="/admin-pages" class="btn btn-secondary " >
                        <i class="fas fa-columns"></i>Stránky</a>       
                    </li>
                    <li>             
                        <a href="/admin-sponsors" class="btn btn-secondary " >
                        <i class="fas fa-ad"></i>Sponzori</a>       
                    </li>
                    @if(Auth::user()->hasRole('admin')) 
                        <li>            
                        <div class="dropdown">
                            <a href="/admin/users" class="btn btn-secondary " >
                        <i class="far fa-user-circle"></i>Účty</a> 
                        
                            
                        </div>
                        </li>
                    @endif 
                    <li>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{Auth::user()->name}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/admin/changeName">Zmeniť meno</a>
                            <a class="dropdown-item" href="/admin/changePassword">Zmeniť heslo</a>
                            <form  id="logout-form" action="{{ url('logout') }}" method="POST">
                                        {{ csrf_field() }}
                                <button  class="dropdown-item"type="submit">Odhlásiť sa</button>
                            </form>
                        </div>
                     </div>       
                    </li>
                </ul>
            </div>   
        </section>
        
        
            <main>
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    @if(Session::has('errors'))
                        <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </div>
                    @endif
                    @yield('content')
            </main>

    </div>
</div>
</body>
</html>
