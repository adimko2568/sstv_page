<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Spojená škola Tvrdošín</title>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700,800&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/scrollreveal"></script>
</head>
<body>
    <div id="app">
        <div class="nav-icon-wrapper">
            <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="hamburger-menu">
            <ul>
                <li><a class="logo" href="/"><img src="{{ asset('assets/logo.png') }}" alt="Spojená škola logo"></a></li>
                @foreach ($menu_items as $key => $menu_item)
                    <li class="burger-item">
                        <a class="" href="#">{{$menu_item->title}}</a>
                        <div class="burger-item-dropdown">
                            @if ($key == 4)
                                <a href="/materialy">Materiály</a>
                                <a href="/odkazy">Odkazy</a>
                            @endif
                            @if ($key == 5)
                                <a href="/internat">Internát</a>
                                <a href="/ubytovna">Ubytovňa</a>
                                <a href="/kontakt">Kontakt</a>
                            @endif
                            @foreach ($menu_item->pages as $subpage)
                                <a class="" href="/page/{{$subpage->slug}}">{{$subpage->title}}</a>
                            @endforeach
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <header class="{{Request::path().'-page-bg'}}">
            <nav class="main-nav">  
                <ul>
                    @foreach ($menu_items as $key => $menu_item)
                        @if ($key <= 2)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="o-skole-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$menu_item->title}}
                                </a>
                                <div class="own-dropdown dropdown-menu" aria-labelledby="o-skole-dropdown">
                                    @foreach ($menu_item->pages as $subpage)
                                        <a class="dropdown-item" href="/page/{{$subpage->slug}}">{{$subpage->title}}</a>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    @endforeach
                    <li><a class="logo" href="/"><img src="{{ asset('assets/logo.png') }}" alt="Spojená škola logo"></a></li>
                    @foreach ($menu_items as $key => $menu_item)
                        @if ($key >= 3)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="o-skole-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$menu_item->title}}
                                </a>
                                <div class="own-dropdown dropdown-menu" aria-labelledby="o-skole-dropdown">
                                    @if ($key == 4)
                                        <a class="dropdown-item" href="/materialy">Materiály</a>
                                        <a class="dropdown-item" href="/odkazy">Odkazy</a>
                                    @endif
                                    @if ($key == 5)
                                        <a class="dropdown-item" href="/internat">Internát</a>
                                        <a class="dropdown-item" href="/ubytovna">Ubytovňa</a>
                                        <a class="dropdown-item" href="/kontakt">Kontakt</a>
                                    @endif
                                    @foreach ($menu_item->pages as $subpage)
                                        <a class="dropdown-item" href="/page/{{$subpage->slug}}">{{$subpage->title}}</a>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </nav>
            <div class="school-banner">
                @yield('header')
            </div>
        </header>
        @if(Auth::check())
        <div class="sideArrow">
            <div class="dropdown">
                <button class="arrowButton"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="arrow-blue-left">Left</i>
                </button>
                
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <form  id="logout-form" action="{{ url('logout') }}" method="POST">
                                {{ csrf_field() }}
                        <button  class="dropdown-item"type="submit">Odhlásiť sa</button>
                    </form>
                </div>
            </div>  
            
            <a href="/admin">{{ Auth::user()->name }}</a>
        </div>
        @endif
        <main>
            @yield('content')
        </main>

        <footer>
            <h1>© <span id="year"></span> All Rights Reserved</h1>
            <h2>Dominik Žuffa, Adam Michalák, Adam Prisenžňák</h2>
            <a href="/admin">Admin</a>
        </footer>
    </div>

</body>
</html>
