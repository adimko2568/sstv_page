@extends('layouts.admin')

@section('content')


<h1 class="text-center"> Upraviť Internát</h1>
  <div class="add-post-form">
      <form action="editInternat" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea  name="content" class="form-control" id="content" rows="6">{!!$internat->content!!}</textarea>
          </div>
          <div class="form-group">
              <label for="food">Jedlo</label>
              <textarea  name="food" class="form-control" id="food" rows="6">{!!$internat->food!!}</textarea>
          </div>
          <div class="form-group">
              <label for="contact">Contact</label>
              <textarea  name="contact" class="form-control" id="contact" rows="6">{!!$internat->contact!!}</textarea>
          </div>
          <div class="form-group">
              <label for="address">Adresa</label>
              <textarea  name="address" class="form-control" id="address" rows="6">{!!$internat->content!!}</textarea>
          </div>


          <div class="form-group">
            <label for="images">Pridať fotky</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Upraviť</button>

      </form>

      <h2>Fotky</h2>

      

        @foreach($internat->internatImages as $image)
        <form action="destroyInternatImage" method="post">
        <div class="post-image">
          <button>X</button>
          <input type="hidden" value="{{$image->id}}" name="id">
          <img src="/public/images/internatImages/{{$image->name}}" alt="">
        </div>
        {{csrf_field()}}
        </form>
        @endforeach

        
      
  </div>

  <script src="{{asset('src/vendor/tinymce/tinymce.min.js')}}"></script>
<script>
 var editor_config = {
      path_absolute : "{{ URL::to('/') }}/",
      selector : "textarea",
      plugins: [
        "advlist autolink lists link charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime  nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.grtElementByTagName('body')[0].clientHeight;
        var cmsURL = editor_config.path_absolute+'laravel-filemanaget?field_name'+field_name;
        if (type = 'image') {
          cmsURL = cmsURL+'&type=Images';
        } else {
          cmsUrl = cmsURL+'&type=Files';
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizeble : 'yes',
          close_previous : 'no'
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endsection