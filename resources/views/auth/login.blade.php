@extends('layouts.login')

@section('content')
    <div class="login-page-wrapper">
        <a class="logo" href="/"><img src="{{ asset('assets/logo.png') }}" alt="Spojená škola logo"></a>
        <form method="POST" action="{{ route('login') }}" class="login-form">
            @csrf

            <h1>Log In</h1>
            <div class="input-wrapper">  
                <label for="name">Prihlasovacie meno</label>

                <div class="">
                    <input id="name" type="text" class="@error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="input-wrapper">
                <label for="password">{{ __('Heslo') }}</label>

                <div class="">
                    <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="input-wrapper">
                <button type="submit">
                    {{ __('Prihlásiť') }}
                </button>
            </div>
        </form>
    </div>
@endsection
