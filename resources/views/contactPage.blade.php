@extends('layouts.app')

@section('header')
    <h1>Kontakt</h1>
    <p>"Emaily, čísla, adresa..."</p>
@endsection

@section('content')
    <div class="contact-wrapper">
        <div class="contact-container">
            <div class="contact-section">
                <h1>SPOJENÁ ŠKOLA TVRDOŠÍN</h1>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/location-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>Medvedzie 133/1</p>
                        <p>027 44 Tvrdošín</p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Phone icon">
                    </div>
                    <div class="contact-item-texts">
                        <h2>sekretariát:</h2>
                        <p>043 58312 10</p>
                        <p>0948 244 961</p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/mail-icon.png') }}" alt="Email icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>
                            <a href="mailto: sekretariat@sstv.sk">sekretariat@sstv.sk</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="contact-section">
                <h1>vrátnica</h1>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 15</p>
                    </div>
                </div>
            </div>

            <div class="contact-section">
                <h1>Školský internát</h1>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 70</p>
                        <p>043 58312 71</p>
                    </div>
                </div>
            </div>

            <div class="contact-section">
                <h2>Vedúca školskej jedálne</h2>
                <p>p. Oľga Duchoňová</p>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 30</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="contact-container">
            <h4>Telefónny zoznam</h4>
            <div class="contact-section">
                <h2>Riaditeľka školy</h2>
                <p>Ing. Ľudmila Uhlíková</p>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 11</p>
                        <p>0948 086 062</p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/mail-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>
                            <a href="mailto: riaditel@spstv.edu.sk">riaditel@spstv.edu.sk</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="contact-section">
                <h2>zástupkyňa riaditeľky školy</h2>
                <p>Ing. Jana Dreveňáková</p>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 12</p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/mail-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>
                            <a href="mailto: drevenakova@spstv.edu.sk">drevenakova@spstv.edu.sk</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="contact-section">
                <h2>výchovná poradkyňa</h2>
                <h2>zástupkyňa riaditeľky školy</h2>
                <p>PaedDr. Tatiana Genšorová</p>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/phone-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>043 58312 13</p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-icon">
                        <img src="{{ asset('assets/mail-icon.png') }}" alt="Location icon">
                    </div>
                    <div class="contact-item-texts">
                        <p>
                            <a href="mailto: gensorova@spstv.edu.sk">gensorova@spstv.edu.sk</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
