@extends('layouts.app')

@section('header')
    <h1>Aktuality</h1>
@endsection



@section('content')
    <div class="all-articles-wrapper">
        @foreach($posts as $post)
            <div class="homepage-article">
                <a href="/prispevok/{{$post->id}}">
                    <h1>{{$post->title}}</h1>
                </a>
                <p class="date">{{date('d.m.Y', strtotime($post->created_at))}}</p>
                <div class="article-texts">
                    <p>{!!$post->short_text()!!}</p>
                </div>
                <div class="article-img">
                    <a href="/public/images/postImages/{{$post->title_image}}" data-lightbox="{{$post->id}}" data-title="{{$post->title}}">
                        <img src="/public/images/postImages/{{$post->title_image}}" alt="">
                    </a>
                </div>
                <div class="article-images-all">
                    @foreach($post->images as $image)
                        <a href="/public/images/postImages/{{$image->name}}" data-lightbox="{{$post->id}}" data-title="{{$post->title}}">fsdaf</a>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
    {{$posts->links()}}
@endsection
