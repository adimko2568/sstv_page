@extends('layouts.app')

@section('header')
    <h2>{{$post->title}}</h2>
@endsection

@section('content')
    <div class="page-container">
        <div class="content">
            {!!$post->content!!}
        </div>
        @if (count($post->images) > 0)
            <div class="gallery-wrapper">
                <h1>Galéria</h1>
                <div class="gallery-container">
                    <div class="gallery-photo">
                        <a href="/public/images/postImages/{{$post->title_image}}" data-lightbox="{{$post->title}}" data-title="{{$post->title}}">
                            <img src="/public/images/postImages/{{$post->title_image}}" alt="">
                        </a>
                    </div>
                    @foreach($post->images as $image)
                        <div class="gallery-photo">
                            <a href="/public/images/postImages/{{$image->name}}" data-lightbox="{{$post->title}}" data-title="{{$post->title}}">
                                <img src="/public/images/postImages/{{$image->name}}" alt="">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="post-thumbnail">
                <a href="/public/images/postImages/{{$post->title_image}}" data-lightbox="{{$post->title}}" data-title="{{$post->title}}">
                    <img src="/public/images/postImages/{{$post->title_image}}" alt="">
                </a>
            </div>
        @endif
    </div>
@endsection
