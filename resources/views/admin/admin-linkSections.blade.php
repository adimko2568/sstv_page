@extends('layouts.admin')

@section('content')
    <h1>Sekcie odkazov <i class=" fas fa-link"></i></h1>

        <div class="addButton"><a href="/add-linkSection"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>

    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Názov sekcie</th>
						<th>Dátum vytvorenia sekcie</th>
                        <th>Dátum úpravy sekcie</th>
                        <th>Upraviť</th>
                        <th>Vymazať</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sections as $section)
                    <tr>
                        <td>{{$section->title}}</td>
						<td>{{date('d.m.Y G:i', strtotime($section->created_at))}}</td>
                        <td>{{date('d.m.Y G:i', strtotime($section->updated_at))}}</td>
                        <td>
                            <a class="btn btn-info" href="/update-linkSection/{{$section->id}}">Upraviť</a>
                        </td>
                        <td>
                        <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$section->id}}" class="btn btn-danger">Vymazať</button>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $sections->links() }}
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať túto sekciu ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyLinkSection" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection