@extends('layouts.admin')

@section('content')
@if(Session::has('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
@endif
<h1 class="text-center"> Pridať súbor <i class=" fas fa-download"></i></h1>
<div class="add-post-form">
    <form action="storeFile" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="col form-group">
                <label for="name">Názov súboru</label>    
                <input value="{{old('name')}}" name="name" type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Názov">
            </div>
            <div class="col form-group">
                <label for="section">Pridať pod sekciu</label>
                <select id="section" class="form-control form-control-sm" name="section">
                @foreach ($sections as $section)
                    <option value="{{$section->id}}">{{$section->sectiontitle}}</option>
                @endforeach
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label for="file">Súbor</label>
            <input name="file" type="file" class="form-control-file" id="file">
        </div>
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary">Odoslať</button>
    </form>   

@endsection
