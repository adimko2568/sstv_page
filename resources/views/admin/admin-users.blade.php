@extends('layouts.admin')

@section('content')
    <h1>Účty <i class="far fa-user-circle"></i></h1>

    
        <div class="addButton"><a href="/register#register"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>

    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Meno</th>
                        <th>Rola</th>
                        <th>Dátum vytvorenia účtu</th>
                        <th>Vymazať účet</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->role}}</td>
                        <td>{{date('d.m.Y G:i', strtotime($user->created_at))}}</td>
                        <td>
                            <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$user->id}}" class="btn btn-danger">Vymazať</button>
                            
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať tento účet ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyUser" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection