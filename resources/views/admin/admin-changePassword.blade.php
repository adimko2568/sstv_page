@extends('layouts.admin')

@section('content')
    <div class="add-post-form">
        <form  action="/changePassword" method="POST">
            <div class="form-group">
                <label for="oldPassword">Staré heslo</label>
                <input required placeholder="Staré heslo" name="oldPassword" type="password" class="form-control" id="oldPassword">
            </div>
            <div class="form-group">
                <label required for="newPassword">Nové heslo</label>
                <input placeholder="Nové heslo" name="newPassword" type="password" class="form-control" id="newPassword">
            </div>
            <div class="form-group">
                <label required for="newPassword2">Zopakovať nové heslo</label>
                <input placeholder="Zopakovať nové heslo" name="newPassword2" type="password" class="form-control" id="newPassword2">
            </div>
            {{csrf_field()}}
            <button type="submit" class="btn btn-primary">Odoslať</button>
        </form>
    </div>
@endsection