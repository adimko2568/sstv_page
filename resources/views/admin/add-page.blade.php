@extends('layouts.admin')

@section('content')
@if(!isset($page))
  <h1 class="text-center"> Pridať stránku</h1>
 <img id="holder" style="margin-top:15px;max-height:100px;">
  <div class="add-post-form">
      <form action="storePage" method="post" enctype="multipart/form-data" id="addPageForm">
          <div class="form-group">
              <label for="title">Názov stránky</label>
              <input value="{{old('pageTitle')}}" name="pageTitle" type="text" class="form-control my-editor" id="title" placeholder="Názov">
          </div>

          <div class="form-group">
              <label for="content">Obsah stránky</label>
              <textarea  name="content" class="form-control my-editor" id="content" rows="15">{{old('content')}}</textarea>
          </div>


          <div class="form-group">
            <label for="images">Vyber fotky</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          <div class="form-group">
            <label for="items">Uložiť pod:</label>
            <select id="items" class="form-control form-control-sm" name="static_menu_item">
                @foreach ($static_menu_items as $item)
                    <option value="{{$item->id}}">{{$item->title}}</option>
                @endforeach
            </select>
          </div>
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>

      <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#addLinkModal">Pridať odkaz</button>
      
      
@else
<h1 class="text-center"> Upraviť stránku</h1>
  <div class="add-post-form">
      <form action="/editPage" method="post" enctype="multipart/form-data" id="addPageForm">

          <div class="form-group">
              <label for="title">Názov</label>
              <input value="{{$page->title}}" name="pageTitle" type="text" class="form-control" id="title" placeholder="Názov">
          </div>

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea  name="content" class="form-control my-editor" id="content" rows="15">{!!$page->content!!}</textarea>
          </div>

          <div class="form-group">
            <label for="images">Pridať fotky</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          <div class="form-group">
            <label for="items">Uložiť pod:</label>
            <select id="items" class="form-control form-control-sm" name="static_menu_item">
                @foreach ($static_menu_items as $item)
                    @if($page->static_menu_item_id == $item->id)
                        <option selected="selected" value="{{$item->id}}">{{$item->title}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->title}}</option>
                    @endif
                @endforeach
            </select>
          </div>
          <input name="id" type="hidden" value="{{$page->id}}">
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>
          
      </form>
      <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#addLinkModal">Pridať odkaz</button>
    <h2>Linky</h2>
     @foreach($page->page_links as $link)
     <form method="POST" action="/destroyPageLink">
      <div class="form-group d-flex link-group">
        
          <input class="form-control mr-3" disabled type='text'  value='{{$link->title}}'>
          <input class="form-control mr-3" disabled type='text'  value='{{$link->url}}'>
          <input type="hidden" name="id" value="{{$link->id}}">
          {{csrf_field()}}
          <button  class="remove-link-btn btn btn-primary">x</button>
        
      </div>
      </form>
    @endforeach
      <h2>Fotky</h2>
        <div class="photos">
        @foreach($page->page_images as $image)
          <div class="photo">
          <form action="/destroyPageImage" method="post">
              <div class="post-image">
              <button  class="close">
                <span aria-hidden="true">&times;</span>
              </button>
              <input type="hidden" value="{{$image->id}}" name="id">
              <img src="/public/images/page_images/{{$image->name}}" alt="">
              </div>
              {{csrf_field()}}
          </form>
          </div>
        
        @endforeach
        </div>
        
      
  </div>
@endif

<div class="modal fade" id="addLinkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Pridať odkaz</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="addLinks">
                <input class="form-control" type="text" name="link-title" placeholder="Názov">
                <input class="form-control" type="text" name="link-url" placeholder="Url">
                <div class="form-group">
                  <label for="file">Alebo súbor (pdf)</label>
                  <input id="link-file" name="files[]" type="file" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Pridať odkaz</button>
              </form>
            </div>
          </div>
        </div>
      </div>
</div>
<script src="/src/vendor/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea",
    language: "sk",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_picker_callback: function (callback, value, meta) {
        let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        let type = 'image' === meta.filetype ? 'Images' : 'Files',
            url  = editor_config.path_absolute + 'laravel-filemanager?editor=tinymce5&type=' + type;

        tinymce.activeEditor.windowManager.openUrl({
            url : url,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            onMessage: (api, message) => {
                callback(message.content);
            }
        });
    }
  };
  
  tinymce.init(editor_config);
</script>

@endsection
