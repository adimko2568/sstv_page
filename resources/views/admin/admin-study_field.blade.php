@extends('layouts.admin')

@section('content')
    

<body>
    <h1>Študíjne odbory <i class="fas fa-atlas"></i></h1>

    
    <div class="addButton"><a href="/add-study_field"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Názov odboru</th>
                        <th>Stránka odboru</th>
						<th>Dátum vytvorenia odboru</th>
                        <th>Dátum úpravy odboru</th>
                        <th>Upraviť</th>
                        <th>Vymazať</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($study_fields as $sf)
                    <tr>
                        <td>{{$sf->title}}</td>
                        <td><a href="/odbor/{{$sf->slug}}">{{$sf->slug}}</a></td>
						<td>{{date('d.m.Y G:i', strtotime($sf->created_at))}}</td>
                        <td>{{date('d.m.Y G:i', strtotime($sf->updated_at))}}</td>
                        <td>
                            <a class="btn btn-info" href="/update-studyField/{{$sf->id}}">Upraviť</a>
                        </td>
                        <td>                    
                            <button  data-toggle="modal" data-target="#confirm-delete" data-id="{{$sf->id}}" class="btn btn-danger">Vymazať</button>
                        </td>
                        
                    </tr>
                    
                    @endforeach
                </tbody>
            </table>
            {{$study_fields->links()}}
        </div>
    </div>
    
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Naozaj chceš vymazať tento odbor ?
            </div>                
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                <form id="delete-form" action="/destroyStudyField" method="POST">
                    <input type="hidden" name="id" value="">
                    <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                </form>
            </div>
        </div>
        </div>
    </div>

    <script>
        console.log('ahoj');
            $('#confirm-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var id = button.data('id');
        modal.find('#delete-form input[name="id"]').val(id);
        console.log($('#delete-form input[name="id"]'));
        })
        
        

    
    </script>
</body>
@endsection