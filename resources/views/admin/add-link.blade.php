@extends('layouts.admin')

@section('content')
<h1 class="text-center"> Pridať odkaz</h1>
<div class="add-post-form">
    <form action="storeLink" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">Názov odkazu</label>    
                <input name="title" value="{{old ('title')}}" type="text" class="form-control" id="title"  placeholder="Názov">
        </div>

        <div class="form-row">
            <div class="col form-group">
                <label for="url">Url</label>    
                    <input name="url" type="url" class="form-control" id="url"  placeholder="Názov">
            </div>
            <div class="col form-group">
                <label for="file">Alebo súbor (pdf)</label>    
                    <input name="file" type="file" class="form-control" id="file">
            </div>
        </div>
        
        <div class="form-group">
            <label for="items">Vyber sekciu</label>
            <select id="items" class="form-control form-control-sm" name="linksection_id">
                @foreach ($linkSections as $section)
                    <option value="{{$section->id}}">{{$section->title}}</option>
                @endforeach
            </select>
          </div>
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary">Odoslať</button>
    </form>   
</div>

@endsection
