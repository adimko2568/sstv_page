@extends('layouts.admin')

@section('content')


@if(isset($post))
  <h1 class="text-center"> Upraviť príspevok</h1>
  <div class="add-post-form">
      <form action="/editPost" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="title">Názov</label>
              <input value="{{$post->title}}" name="title" type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Názov">
          </div>

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea  name="content" class="form-control" id="content" rows="15">{!!$post->content!!}</textarea>
          </div>

          <div class="form-group">
            <label for="titleImage">Zmeniť titulnú fotku</label>
            <input name="titleImage" type="file" class="form-control-file" id="titleImage">
          </div>

          <div class="form-group">
            <label for="images">Pridať fotky</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          <input name="id" type="hidden" value="{{$post->id}}">
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>

      <h2>Fotky</h2>

      
        <div class="photos">
        @foreach($post->images as $image)
          <div class="photo">
            <form action="/destroyPostImage" method="post">
            <div class="post-image">
              <button  class="close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <input type="hidden" value="{{$image->id}}" name="id">
              <img src="/public/images/postImages/{{$image->name}}" alt="">
            </div>
            {{csrf_field()}}
            </form>
          </div>
        @endforeach
        </div>

       
      
  </div>
@else
  <h1 class="text-center"> Pridať príspevok</h1>
  <div class="add-post-form">
      <form action="storePost" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="title">Názov</label>
              <input value="{{ old('title') }}" name="title" type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Názov">
          </div>

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea name="content" class="form-control" id="content" rows="15">{{ old('content') }}</textarea>
          </div>

          <div class="form-group">
            <label for="titleImage">Vyber náhľadovú fotku</label>
            <input name="titleImage" type="file" class="form-control-file" id="titleImage">
          </div>

          <div class="form-group">
            <label for="images">Vyber fotky do gelérie</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>
  </div>
@endif
<script src="{{asset('src/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
 var editor_config = {
      path_absolute : "/",
      selector : "textarea",
      language: "sk",
      plugins: [
        "advlist autolink lists link charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime  nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.grtElementByTagName('body')[0].clientHeight;
        var cmsURL = editor_config.path_absolute+'laravel-filemanaget?field_name'+field_name;
        if (type = 'image') {
          cmsURL = cmsURL+'&type=Images';
        } else {
          cmsUrl = cmsURL+'&type=Files';
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizeble : 'yes',
          close_previous : 'no'
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endsection
