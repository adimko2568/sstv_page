@extends('layouts.admin')

@section('content')
    <h1>Príspevky</h1>
    <div class="addButton"><a href="/add-post"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    <div class="all-articles-wrapper">
        <div class="row">
            @foreach($posts as $post)
            <div class="col">
                <div class="homepage-article">
                    <a href="/prispevok/{{$post->id}}">
                        <h1>{{$post->title}}</h1>
                    </a>
                    <p class="date">{{date('d.m.Y', strtotime($post->created_at))}}</p>
                    <div class="article-texts">
                        <p>{!!$post->short_text()!!}</p>
                    </div>
                    <div class="article-img">
                        <img src="/public/images/postImages/{{$post->title_image}}" alt="">
                    </div>
                    <div class="buttons">
                        <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$post->id}}" class="btn btn-danger">Zmazať príspevok</button>
                        <a href="/update-post/{{$post->id}}" class="btn btn-info">Upraviť príspevok</a>
                    </div>
                </div>     
            </div>
            @endforeach
        </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať tento príspevok ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyPost" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                            {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
    $('a[data-confirm]').click(function(ev) {
        var href = $(this).attr('href');

        if (!$('#dataConfirmModal').length) {
            $('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
        } 
        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show:true});
        return false;
    });
});

$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
    </script>
@endsection