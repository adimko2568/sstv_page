@extends('layouts.admin')

@section('content')
    <h1>Linky <i class=" fas fa-link"></i></h1>

    
        
        <div class="addButton"><a href="/add-link"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    @foreach ($linkSections as $section)
    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Názov</th>
                        <th>Url</th>
                        <th>Sekcia odkazu</th>
                        <th>Odstraniť link</th>
                    </tr>
                </thead>
                <tbody>
                    
                        <h2>{{$section->title}}</h2>
                    @foreach($section->links  as $link)
                    <tr>
                        <td>{{$link->title}}</td>
                        <td><a target="_blank" href="{{$link->url}}">{{$link->url}}</a></td>
                        <td>{{$section->title}}</td>
                        <td>
                            <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$link->id}}" class="btn btn-danger">Vymazať</button>
                        </td>
                        
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
    @endforeach
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať tento link ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyLink" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection