@extends('layouts.admin')

@section('content')
    <h1>Stránky <i class="fas fa-columns"></i></h1>

    
    <div class="addButton"><a href="/add-page"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    @foreach ($static_pages as $static_page)
    <div class="container-fluid">
        <div class="table-wrapper">
        
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Názov stránky</th>
                        <th>Stránka</th>
						<th>Dátum vytvorenia stránky</th>
                        <th>Dátum úpravy stránky</th>
                        <th>Upraviť</th>
                        <th>Vymazať</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <h2>{{$static_page->title}}</h2>
                    @foreach($static_page->pages as $page)
                    
                    <tr>
                        <td>{{$page->title}}</td>
                        <td><a href="/page/{{$page->slug}}">{{$page->slug}}</a></td>
						<td>{{date('d.m.Y G:i', strtotime($page->created_at))}}</td>
                        <td>{{date('d.m.Y G:i', strtotime($page->updated_at))}}</td>
                        <td>
                            <a class="btn btn-info" href="/update-page/{{$page->id}}">Upraviť</a>
                        </td>
                        <td>
                            <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$page->id}}" class="btn btn-danger">Vymazať</button>
                        </td>
                        
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            
        </div>
    </div>
    @endforeach
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať túto stránku ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyPage" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection