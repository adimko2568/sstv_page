@extends('layouts.admin')

@section('content')
<h1 class="text-center"> Pridať sekciu</h1>
<div class="add-post-form">
@if(!isset($section))
    <form action="storeSectionLink" method="post">
        <div class="form-group">
            <label for="title">Názov sekcie</label>    
                <input value="{{old('title')}}" name="title" type="text" class="form-control" id="title"  placeholder="Názov">
        </div>
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary">Odoslať</button>
    </form>   
@else 
    <form action="/editLinkSection" method="post">
        <div class="form-group">
            <label for="title">Názov sekcie</label>
            <input value="{{$section->title}}" name="title" type="text" class="form-control" id="title" placeholder="Názov">
            <input value="{{$section->id}}" name="id" type="hidden">      
        </div>
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary">Upraviť</button>
    </form>
@endif
</div>

@endsection
