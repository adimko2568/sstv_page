@extends('layouts.admin')

@section('content')



  <h1 class="text-center">Pridať sponzora</h1>
  <div class="add-post-form">
      <form action="/storeSponsor" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="title">Url Sponzora</label>
              <input value="{{old('sponsorUrl')}}"  name="sponsorUrl" type="url" class="form-control" id="title"  placeholder="Url">
          </div>
          
          <div class="form-group">
            <label for="titleImage">Fotka sponzora</label>
            <input name="img" type="file" class="form-control-file" id="titleImage">
          </div>

          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>

  </div>

@endsection
