@extends('layouts.admin')

@section('content')


@if(!isset($study_field))
  <h1 class="text-center"> Pridať odbor</h1>
  <div class="add-post-form">
      <form action="storeStudyField" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="title">Názov</label>
              <input name="title" type="text" class="form-control" id="title"placeholder="Názov" value="{{ old('title') }}">
          </div>

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea name="content" class="form-control" id="content" rows="15">{{ old('content') }}</textarea>
          </div>

          <div class="form-group">
            <label for="titleImage">Vyber logo odboru</label>
            <input name="icon" type="file" class="form-control-file" id="titleImage">
          </div>

          <div class="form-group">
            <label for="images">Vyber fotky do galérie</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>
      
      @else
      <h1 class="text-center"> Zmeniť odbor</h1>
      <div class="add-post-form">
      <form action="{{ route('editStudyField') }}" method="post" enctype="multipart/form-data">

          <div class="form-group">
              <label for="title">Názov</label>
              <input value="{{$study_field->title}}" name="title" type="text" class="form-control" id="title"  placeholder="Názov">
              
          </div>

          <div class="form-group">
              <label for="content">Obsah</label>
              <textarea name="content" class="form-control" id="content" rows="15">{{$study_field->content}}</textarea>
          </div>

          <div class="form-group">
            <label for="titleImage">Zmeň logo odboru</label>
            <input name="icon" type="file" class="form-control-file" id="titleImage">
          </div>

          <div class="form-group">
            <label for="images">Pridaj fotky k odboru</label>
            <input name="images[]" type="file" class="form-control-file" id="images" multiple>
          </div>
          <input type="hidden" name="id" value="{{$study_field->id}}">
          {{csrf_field()}}
          <button type="submit" class="btn btn-primary">Odoslať</button>

      </form>

      <h2>Fotky</h2>

      
        <div class="photos">
        @foreach($study_field->study_field_images as $image)
        <div class="photo">
        <form action="/destroyStudyFieldImage" method="post">
            <div class="post-image">
            <button  class="close">
                <span aria-hidden="true">&times;</span>
            </button>
            <input type="hidden" value="{{$image->id}}" name="id">
            <img src="/public/images/study_field_images/{{$image->name}}" alt="">
            </div>
            {{csrf_field()}}
        </form>
        </div>
        @endforeach
        </div>
  @endif
  </div>
<script src="{{asset('src/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea",
    language: "sk",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_picker_callback: function (callback, value, meta) {
        let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        let type = 'image' === meta.filetype ? 'Images' : 'Files',
            url  = editor_config.path_absolute + 'laravel-filemanager?editor=tinymce5&type=' + type;

        tinymce.activeEditor.windowManager.openUrl({
            url : url,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            onMessage: (api, message) => {
                callback(message.content);
            }
        });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection
