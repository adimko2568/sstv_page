@extends('layouts.admin')

@section('content')
    <div class="add-post-form">
        <form  action="/changeName" method="POST">
            <div class="form-group">
                <label for="title">Meno</label>
                <input required value="{{$user->name}}" name="name" type="text" class="form-control" id="title">
            </div>
            {{csrf_field()}}
            <button type="submit" class="btn btn-primary">Odoslať</button>
        </form>
    </div>
@endsection