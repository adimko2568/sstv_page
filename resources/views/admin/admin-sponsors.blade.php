@extends('layouts.admin')

@section('content')
    <h1>Sponzori <i class="fas fa-ad"></i></h1>

    
    <div class="addButton"><a href="/add-sponsor"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Url Sponzora</th>
                        <th>Vymazať</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sponsors as $sponsor)
                    <tr>
                        <td><a href="{{$sponsor->sponsorUrl}}">{{$sponsor->sponsorUrl}}</a></td>
                        <td>
                            <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$sponsor->id}}" class="btn btn-danger">Vymazať</button>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $sponsors->links() }}
        </div>
    </div>


    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať tohoto sponzora ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroySponsor" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection