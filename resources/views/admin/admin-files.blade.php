@extends('layouts.admin')

@section('content')
    <h1>Súbory <i class=" fas fa-download"></i></h1>
        
        <div class="addButton"><a href="/add-file"><img src="{{asset('images/admin/plus.svg')}}" alt="add-button"></a></div>
    @foreach ($sections as $section)
    <div class="container-fluid">
        <div class="table-wrapper">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Názov</th>
                        <th>Názov súboru</th>
                        <th>Sekcia súboru</th>
						<th>Typ súboru</th>
                        <th>Veľkosť súboru</th>
                        <th>Vymazať</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <h2>{{$section->sectiontitle}}</h2>
                    @foreach($section->files  as $file)
                    <tr>
                        <td>{{$file->name}}</td>
                        <td>{{$file->title}}</td>
                        <td>{{$section->sectiontitle}}</td>
						<td>{{$file->ext}}</td>
                        <td>{{$file->size / 1000}} kB</td>
                        <td>
                            <button data-toggle="modal" data-target="#confirm-delete" data-id="{{$file->id}}" class="btn btn-danger">Odstrániť súbor</button>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endforeach
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Naozaj chceš vymazať tento súbor ?
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                    <form id="delete-form" action="/destroyFile" method="POST">
                        <input type="hidden" name="id" value="">
                        <button class="btn btn-danger">Vymazať</button>
                        {{ csrf_field() }}  
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection