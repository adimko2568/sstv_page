<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 255);
            $table->string('name', 255);
            $table->string('ext' ,10);
            $table->bigInteger('size');
            $table->bigInteger('filesection_id')->unsigned();
            $table->index('filesection_id');
            $table->foreign('filesection_id')->references('id')->on('filesections')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
